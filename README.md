# FLISoL Parauapebas 2019

## Minicurso GNU/Linux para Desktop

Minicurso de introdução ao uso de Distribuições GNU/Linux para Desktop.


## Conteúdo do minicurso

1. Introdução: Conceitos Fundamentais
   1. Sistema Operacional
   1. O quê é o Linux
   1. O quê é o GNU/Linux
   1. Distribuição Linux
   1. Ambiente de Desktop
   1. Filosofia Unix
   1. Filesystem Hierarchy Standard (Árvore de Diretórios)
   1. Obtendo, Verificando, Gravando e Instalando
1. Linha de comando
   1. O quê é Shell
   1. Bash
   1. Sintaxe básica e conseguindo ajuda (man, info, --help)
   1. Entrada, Saídas padrões e redirecionamentos
   1. Variáveis de ambiente
   1. Caminho absoluto e caminho relativo
   1. Entradas, Saídas e Redirecionamento de Streams de Texto
1. Gerenciamento de Pacotes
   1. Sistema de Gerenciamento de Pacotes
   1. O quê são pacotes
   1. Fontes de Software ou Repositórios de Pacotes
   1. Tipos de Pacotes
   1. Gerenciamento básico de pacotes na GUI
   1. Gerenciamento básico de pacotes na CLI
1. Compartilhamento de Arquivos com SAMBA
   1. O quê o SAMBA
   1. Acessando compartilhamento no gerenciador de arquivos
   1. Compartilhando arquivos no gerenciador de arquivos
1. CUPS: Servidor de Impressão
   1. O quê é o CUPS
   1. Gerenciamento básico de impressoras com system-config-printer
   1. Impressoras Multifuncionais da HP e HPLIP
   1. Gerenciamento básico de impressoras na web GUI
   1. Drivers extras e o openprinting.org
1. Configuração de rede
   1. O quê é o Network Manager
   1. Configurando a rede na GUI (applet do NM)
1. Dicas e Truques
   1. PPAs e cuidados com pacotes de terceiros
   1. Instalação de drivers extras (Nvidia, AMD/ATI e etc)


## License

GPLv3 or later.
